<?php


namespace BinaryStudioAcademy\Game\View;


use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\AlienShip;

class GalaxyInfo
{
    private CliWriter $writer;

    public function __construct(protected AlienShip $ship, protected string $galaxy) {
        $this->writer = new CliWriter();
    }

    public function showGalaxyInfo()
    {
        if ($this->galaxy === 'Home') {
            $this->homeGalaxyInfo();
        }else {
            $this->alienGalaxyInfo($this->ship, $this->galaxy);
        }

    }

    public function homeGalaxyInfo()
    {
        $this->writer->writeln('Galaxy: Home Galaxy.');
    }

    public function alienGalaxyInfo(AlienShip $ship, $galaxy)
    {
        $this->writer->writeln("Galaxy: {$galaxy}.");
        (new ShipInfo($ship))->showAlienStats();
    }

}

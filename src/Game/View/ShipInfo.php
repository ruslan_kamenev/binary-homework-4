<?php


namespace BinaryStudioAcademy\Game\View;


use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\Ship;

class ShipInfo
{
    private CliWriter $writer;

    public function __construct(protected Ship $ship) {
        $this->writer = new CliWriter();
    }

    public function showPlayerStats(): void
    {
        $this->writer->writeln('Spaceship stats:');
        $this->showAllStats();
        $this->showCargo();
    }

    public function showAlienStats(): void
    {
        $this->showShipName();
        $this->showAllStats();
    }


    public function showAllStats(): void
    {
        $this->writer->writeln('strength: ' . $this->ship->getStrength());
        $this->writer->writeln('armor: ' . $this->ship->getArmor());
        $this->writer->writeln('luck: ' . $this->ship->getLuck());
        $this->writer->writeln('health: ' . $this->ship->getHealth());
    }

    public function showCargo(): void
    {
        if (count($this->ship->getItems()) > 0) {
            foreach ($this->ship->getItems() as $item) {
                $this->writer->writeln('hold: ' . $item);
            }
        } else {
            $this->writer->writeln('[ _ _ _ ]');
        }
    }

    public function showShipName()
    {
        $this->writer->writeln("You see a {$this->ship->getName()}: ");
    }

}

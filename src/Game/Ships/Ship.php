<?php


namespace BinaryStudioAcademy\Game\Ships;

abstract class Ship
{
    public function __construct(
        protected int $strength,
        protected int $armor,
        private int $luck,
        private int $health,
        protected array $hold
    ) {
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function getArmor(): int
    {
        return $this->armor;
    }
    public function getLuck(): int
    {
        return $this->luck;
    }
    public function getHealth(): int
    {
        return $this->health;
    }

    public function getItems(): array
    {
        return $this->hold;
    }

    public function setHealth($health)
    {
        $this->health = $health;
    }
}

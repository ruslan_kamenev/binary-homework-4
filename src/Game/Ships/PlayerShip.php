<?php


namespace BinaryStudioAcademy\Game\Ships;


class PlayerShip extends Ship
{
    private string $galaxy;

    public function __construct(int $strength, int $armor, int $luck, int $health, array $hold, string $galaxy = 'Home')
    {
        parent::__construct($strength, $armor, $luck, $health, $hold);
        $this->galaxy = $galaxy;
    }

    public function getGalaxy(): string
    {
        return $this->galaxy;
    }

    public function setGalaxy($galaxyName)
    {
        $this->galaxy = $galaxyName;
    }

    public function addItem($item)
    {
        $this->hold[] = $item;
    }

    public function removeItem($item)
    {
        $key = array_search($item, $this->hold);
        unset($this->hold[$key]);
    }

    public function setStrength($amount)
    {
        $this->strength = $amount;
    }

    public function setArmor($amount)
    {
        $this->armor = $amount;
    }
}

<?php


namespace BinaryStudioAcademy\Game\Ships;


class AlienShip extends Ship
{
    private string $name;
    private string $status;

    public function __construct(int $strength, int $armor, int $luck, int $health, array $hold, string $name = 'Home')
    {
        parent::__construct($strength, $armor, $luck, $health, $hold);
        $this->name = $name;
        $this->status = 'alive';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function emptyHold()
    {
        $this->hold = [];
    }

    public function changeStatus()
    {
        $this->status = 'dead';
    }
}

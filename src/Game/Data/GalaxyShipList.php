<?php

return [
    'Home' => 'None',
    'Andromeda' => 'Patrol',
    'Pegasus' => 'Patrol',
    'Spiral' => 'Patrol',
    'Shiar' => 'Battle',
    'Xeno' => 'Battle',
    'Isop' => 'Executor'
];

<?php

return [
    'Patrol' => [
        'strength' => rand(3, 4),
        'armor' => rand(2, 3),
        'luck' => rand(1, 2),
        'health' => 100,
        'name' => 'Patrol Spaceship',
        'hold' => [
            'reactor'
        ]
    ],

    'Battle' => [
        'strength' => rand(5, 8),
        'armor' => rand(6, 8),
        'luck' => rand(3, 6),
        'health' => 100,
        'name' => 'Battle Spaceship',
        'hold' => [
            'reactor',
            'cristal'
        ]
    ],

    'Executor' => [
        'strength' => 10,
        'armor' => 10,
        'luck' => 10,
        'health' => 100,
        'name' => 'Executor',
        'hold' => [
            'reactor',
            'cristal',
            'cristal'
        ]
    ]
];

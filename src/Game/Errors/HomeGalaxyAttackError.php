<?php


namespace BinaryStudioAcademy\Game\Errors;


use BinaryStudioAcademy\Game\Io\CliWriter;

class HomeGalaxyAttackError
{
    static function noEnemyError(): void
    {
        (new CliWriter())->writeln('Calm down! No enemy spaceships detected. No one to fight with.');
    }
}

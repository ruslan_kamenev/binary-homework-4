<?php


namespace BinaryStudioAcademy\Game\Errors;


use BinaryStudioAcademy\Game\Io\CliWriter;

class NoItemGrabError
{
    static function noItems(): void
    {
        (new CliWriter())->writeln('Nothing to grab');
    }
}

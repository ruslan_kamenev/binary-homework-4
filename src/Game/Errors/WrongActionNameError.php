<?php

namespace BinaryStudioAcademy\Game\Errors;

class WrongActionNameError
{
    public function __construct () {}

    public function noSuchCommandMessage(string $input): string
    {
        return "Command '{$input}' not found";
    }
}

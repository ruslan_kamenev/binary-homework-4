<?php


namespace BinaryStudioAcademy\Game\Errors;


use BinaryStudioAcademy\Game\Io\CliWriter;

class AliveAlienGrabError
{
    static function aliveAlien(): void
    {
        (new CliWriter())->writeln('LoL. Unable to grab goods. Try to destroy enemy spaceship first.');
    }
}

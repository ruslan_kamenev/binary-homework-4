<?php


namespace BinaryStudioAcademy\Game\Errors;


use BinaryStudioAcademy\Game\Io\CliWriter;

class AlienAlreadyDestroyedError
{
    static function alienDestroyed(): void
    {
        (new CliWriter())->writeln('Alien ship already destroyed');
    }
}

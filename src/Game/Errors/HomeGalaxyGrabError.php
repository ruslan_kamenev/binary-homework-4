<?php


namespace BinaryStudioAcademy\Game\Errors;


use BinaryStudioAcademy\Game\Io\CliWriter;

class HomeGalaxyGrabError
{
    static function error(): void
    {
        (new CliWriter())->writeln('Hah? You don\'t want to grab any staff at Home Galaxy. Believe me.');
    }
}

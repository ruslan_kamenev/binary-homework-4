<?php


namespace BinaryStudioAcademy\Game\Actions;


use BinaryStudioAcademy\Game\Helpers\PlayerShipStats;
use BinaryStudioAcademy\Game\Ships\PlayerShip;

class CreateNewPlayerShip
{
    static function newShip(): PlayerShip
    {
        return new PlayerShip(
            PlayerShipStats::STRENGTH,
            PlayerShipStats::ARMOR,
            PlayerShipStats::LUCK,
            PlayerShipStats::HEALTH,
            PlayerShipStats::CARGO);
    }
}

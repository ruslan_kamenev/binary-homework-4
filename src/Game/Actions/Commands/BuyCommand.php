<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Helpers\PlayerMaxStats;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\PlayerShip;
use function Composer\Autoload\includeFile;

class BuyCommand
{
    static function buy(PlayerShip $playerShip, string $item): void
    {
        switch ($item) {
            case 'strength':
                self::buyStrength($playerShip);
                break;
            case 'armor':
                self::buyArmor($playerShip);
                break;
            case 'reactor':
                self::buyReactor($playerShip);
                break;
            default:
                (new CliWriter())->writeln('Wrong item|stats name');
        }
    }

    static function buyStrength(PlayerShip $playerShip): void
    {
        if (self::buyPossible($playerShip)) {
            $playerShip->setStrength($playerShip->getStrength() + 1);
            if ($playerShip->getStrength() > PlayerMaxStats::STRENGTH) {
                $playerShip->setStrength(10);
            }
            (new CliWriter())->writeln("You\'ve got upgraded skill: strength. The level is {$playerShip->getStrength()} now.");
        }
    }

    static function buyArmor(PlayerShip $playerShip): void
    {
        if (self::buyPossible($playerShip)) {
            $playerShip->setArmor($playerShip->getArmor() + 1);
            if ($playerShip->getArmor() > PlayerMaxStats::ARMOR) {
                $playerShip->setArmor(10);
            }
            (new CliWriter())->writeln("You\'ve got upgraded skill: armor. The level is {$playerShip->getArmor()} now.");
        }
    }


    static function buyReactor(PlayerShip $playerShip): void
    {
        if (self::buyPossible($playerShip)) {
            $playerShip->setArmor($playerShip->getArmor() + 1);
            $playerShip->addItem('reactor');
            $reactorsAmount = count(array_keys($playerShip->getItems(), 'reactor'));
            (new CliWriter())->writeln("You\'ve bought a magnet reactor. You have {$reactorsAmount} reactor(s) now.");
        }
    }

    static function buyPossible(PlayerShip $playerShip): bool
    {
        if ($playerShip->getGalaxy() !== 'Home') {
            (new CliWriter())->writeln('You can buy items only in Home galaxy');
            return False;
        }
        if (in_array('cristal', $playerShip->getItems())) {
            return True;
        } else {
            (new CliWriter())->writeln('You got not enough crystals');
            return False;
        }
    }
}

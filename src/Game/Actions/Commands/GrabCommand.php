<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Errors\AliveAlienGrabError;
use BinaryStudioAcademy\Game\Errors\NoItemGrabError;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\AlienShip;
use BinaryStudioAcademy\Game\Ships\PlayerShip;
use phpDocumentor\Reflection\Types\True_;

class GrabCommand
{
    static function grab(PlayerShip $playerShip, AlienShip $alienShip): void
    {
        if ($playerShip->getGalaxy() === 'Home') {
            (new CliWriter())->writeln('Hah? You don\'t want to grab any staff at Home Galaxy. Believe me.');
            exit();
        }

         match ($alienShip->getName()) {
            'Patrol Spaceship' => self::grabPatrol($playerShip),
            'Battle Spaceship' => self::grabBattleship($playerShip)
        };
        $alienShip->emptyHold();
    }

    static function grabPatrol(PlayerShip $playerShip): void
    {
        $playerShip->addItem('reactor');
        (new CliWriter())->writeln('You got 🔋.');
    }

    static function grabBattleship(PlayerShip $playerShip): void
    {
        $playerShip->addItem('reactor');
        $playerShip->addItem('cristal');
        (new CliWriter())->writeln('You got 🔋 🔮.');
    }

    static function grabPossibilityCheck(PlayerShip $playerShip, AlienShip $alienShip): bool
    {
        if (self::homeGrabCheck($playerShip)){
            return True;
        }

        if ($alienShip->getHealth() > 0) {
            AliveAlienGrabError::aliveAlien();
            return True;
        }
        if (count($alienShip->getItems()) <= 0) {
            NoItemGrabError::noItems();
            return True;
        }

        return False;
    }

    static function homeGrabCheck(PlayerShip $playerShip): bool
    {
        if ($playerShip->getGalaxy() === 'Home') {
            return True;
        } else {
            return False;
        }
    }

}

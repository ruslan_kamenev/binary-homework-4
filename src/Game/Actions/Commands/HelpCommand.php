<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;

use BinaryStudioAcademy\Game\Io\CliWriter;

class HelpCommand
{
    private CliWriter $writer;

    public function __construct() {
        $this->writer = new CliWriter;
    }

    public function showAllCommands()
    {
        $this->writer->writeln('List of commands:');
        $this->writer->writeln('help - shows this list of commands');
        $this->writer->writeln('stats - shows stats of spaceship');
        $this->writer->writeln('set-galaxy <home|andromeda|spiral|pegasus|shiar|xeno|isop> - provides jump into specified galaxy');
        $this->writer->writeln('attack - attacks enemy\'s spaceship');
        $this->writer->writeln('grab - grab useful load from the spaceship');
        $this->writer->writeln('buy <strength|armor|reactor> - buys skill or reactor (1 item)');
        $this->writer->writeln('apply-reactor - apply magnet reactor to increase spaceship health level on 20 points');
        $this->writer->writeln('whereami - shows current galaxy');
        $this->writer->writeln('restart - restarts game');
        $this->writer->writeln('exit - ends the game');
    }
}

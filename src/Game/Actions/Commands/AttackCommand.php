<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Errors\AlienAlreadyDestroyedError;
use BinaryStudioAcademy\Game\Errors\HomeGalaxyAttackError;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\AlienShip;
use BinaryStudioAcademy\Game\Ships\PlayerShip;
use BinaryStudioAcademy\Game\Ships\Ship;

class AttackCommand
{
    static function fightBetweenShips(PlayerShip $playerShip, AlienShip $alienShip, $random)
    {
        $write = new CliWriter();
        if (self::hitChance($playerShip, $random)) {
            $damage = self::getDamage($playerShip, $alienShip);
            self::setDamage($alienShip, $damage);
            $write->writeln("{$alienShip->getName()} has damaged on: {$damage} points.");
            $write->writeln("health: {$alienShip->getHealth()}");


        }
        if (self::hitChance($alienShip, $random)) {
            $damage = self::getDamage($alienShip, $playerShip);
            self::setDamage($playerShip, $damage);
            $write->writeln("{$alienShip->getName()} damaged your spaceship on: {$damage} points.");
            $write->writeln("health: {$playerShip->getHealth()}");
        }
    }

    static function hitChance(Ship $ship, $random): bool
    {
        $math = new Math();
        return $math->luck($random, $ship->getLuck());
    }

    static function getDamage(Ship $attackShip, Ship $defenderShip): int
    {
        $math = new Math();
        return $math->damage($attackShip->getStrength(), $defenderShip->getArmor());
    }

    static function setDamage(Ship $ship, int $damage): void
    {
        $ship->setHealth($ship->getHealth() - $damage);
    }

    static function attackPossible(PlayerShip $playerShip, AlienShip $alienShip): bool
    {
        if ($playerShip->getGalaxy() === 'Home') {
            HomeGalaxyAttackError::noEnemyError();
            return False;
        }
        if ($alienShip->getHealth() <= 0) {
            AlienAlreadyDestroyedError::alienDestroyed();
            return False;
        }
        return True;
    }

}

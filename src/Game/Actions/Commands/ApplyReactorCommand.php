<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Helpers\PlayerMaxStats;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\PlayerShip;

class ApplyReactorCommand
{
    static function applyReactor(PlayerShip $playerShip): void
    {
        $writer = new CliWriter();
        if (in_array('reactor', $playerShip->getItems())) {
            $playerShip->removeItem('reactor');
            $playerShip->setHealth($playerShip->getHealth() + 20);

            if ($playerShip->getHealth() > PlayerMaxStats::HEALTH){
                $playerShip->setHealth(100);
            }
            $writer->writeln("Magnet reactor have been applied. Current spaceship health level is {$playerShip->getHealth()}");
        } else {
            $writer->writeln('No reactor in cargo found');
        }
    }
}

<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Game;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\PlayerShip;

class SetGalaxyCommand
{
    static function changePlayerGalaxy(PlayerShip $ship, string $galaxy): void
    {
        $galaxyList = include Game::ROOT_PATH.'/Data/GalaxyShipList.php';
        $galaxy = ucfirst($galaxy);
        if (self::galaxyExistCheck($galaxy, $galaxyList)) {
            $ship->setGalaxy($galaxy);
        } else {
            self::wrongGalaxyName();
        }
    }

    static function galaxyExistCheck($galaxy, $galaxyList): bool
    {
        if (array_key_exists($galaxy, $galaxyList)) {
            return True;
        } else {
            return False;
        }
    }

    static function wrongGalaxyName(): void
    {
        $writer = new CliWriter();
        $writer->writeln('Nah. No specified galaxy found.');
    }
}

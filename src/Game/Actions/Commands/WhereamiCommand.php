<?php


namespace BinaryStudioAcademy\Game\Actions\Commands;


use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\PlayerShip;

class WhereamiCommand
{
    static function position(PlayerShip $playerShip): void
    {
        (new CliWriter())->writeln("Galaxy: {$playerShip->getGalaxy()}");
    }
}

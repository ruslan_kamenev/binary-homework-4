<?php


namespace BinaryStudioAcademy\Game\Actions;


use BinaryStudioAcademy\Game\Helpers\AlienShipTypeChose;
use BinaryStudioAcademy\Game\Helpers\GetAlienShipStats;
use BinaryStudioAcademy\Game\Ships\AlienShip;

class CreateNewAlienShip
{
    static function createShip(string $galaxy): AlienShip
    {
        $alienShipType = AlienShipTypeChose::shipTypeChose($galaxy);
        $alienShipStats = GetAlienShipStats::getShipStats($alienShipType);

        return new AlienShip(
            $alienShipStats['strength'],
            $alienShipStats['armor'],
            $alienShipStats['luck'],
            $alienShipStats['health'],
            $alienShipStats['hold'],
            $alienShipStats['name']
        );
    }
}

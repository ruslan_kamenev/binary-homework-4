<?php

namespace BinaryStudioAcademy\Game\Actions;

use BinaryStudioAcademy\Game\Actions\Commands\ApplyReactorCommand;
use BinaryStudioAcademy\Game\Actions\Commands\AttackCommand;
use BinaryStudioAcademy\Game\Actions\Commands\BuyCommand;
use BinaryStudioAcademy\Game\Actions\Commands\GrabCommand;
use BinaryStudioAcademy\Game\Actions\Commands\HelpCommand;
use BinaryStudioAcademy\Game\Actions\Commands\SetGalaxyCommand;
use BinaryStudioAcademy\Game\Actions\Commands\WhereamiCommand;
use BinaryStudioAcademy\Game\Errors\HomeGalaxyAttackError;
use BinaryStudioAcademy\Game\Errors\HomeGalaxyGrabError;
use BinaryStudioAcademy\Game\Errors\WrongActionNameError;
use BinaryStudioAcademy\Game\Events\ShipDestroyedCheck;
use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\AlienShip;
use BinaryStudioAcademy\Game\Ships\PlayerShip;
use BinaryStudioAcademy\Game\View\GalaxyInfo;
use BinaryStudioAcademy\Game\View\ShipInfo;

class UserChooseAction
{
    /**
     * @var string[]
     */
    private array $userInputArray;
    private AlienShip $alienShip;


    public function __construct(
        private PlayerShip $playerShip,
        private $random
    ) {
    }

    public function makeAction(): void
    {
        switch ($this->userInputArray[0]){
            case 'help':
                (new HelpCommand())->showAllCommands();
                break;

            case 'stats':
                (new ShipInfo($this->playerShip))->showPlayerStats();
                break;

            case 'set-galaxy':
                SetGalaxyCommand::changePlayerGalaxy($this->playerShip, $this->userInputArray[1]);
                $this->alienShip = CreateNewAlienShip::createShip($this->playerShip->getGalaxy());
                (new GalaxyInfo($this->alienShip, $this->playerShip->getGalaxy()))->showGalaxyInfo();
                break;

            case 'attack':
                if (isset($this->alienShip)) {
                    if (AttackCommand::attackPossible($this->playerShip, $this->alienShip)) {
                        AttackCommand::fightBetweenShips($this->playerShip, $this->alienShip, $this->random);
                    }
                    ShipDestroyedCheck::finishCheck($this->alienShip);
                }else {
                    HomeGalaxyAttackError::noEnemyError();
                }
                break;

            case 'grab':
                if (GrabCommand::homeGrabCheck($this->playerShip)) {
                    HomeGalaxyGrabError::error();
                    break;
                }
                if (isset($this->alienShip)){
                    if (GrabCommand::grabPossibilityCheck($this->playerShip, $this->alienShip)){
                        break;
                    }
                    GrabCommand::grab($this->playerShip, $this->alienShip);
                }
                break;

            case 'buy':
                BuyCommand::buy($this->playerShip, $this->userInputArray[1]);
                break;

            case 'apply-reactor':
                ApplyReactorCommand::applyReactor($this->playerShip);
                break;

            case 'whereami':
                WhereamiCommand::position($this->playerShip);
                break;

            case 'restart':
                $this->playerShip = CreateNewPlayerShip::newShip();
                break;

            default:
                (new CliWriter())->writeLn((new
                WrongActionNameError())->noSuchCommandMessage($this->userInputArray[0]));
        }

        if (ShipDestroyedCheck::playerShipHealthCheck($this->playerShip)) {
            $this->playerShip = CreateNewPlayerShip::newShip();
        }
    }

    public function prepareUserInput($input)
    {
        $input = trim($input, ' ');
        $this->userInputArray = explode(' ', $input);
    }
}

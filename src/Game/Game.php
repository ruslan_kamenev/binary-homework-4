<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Actions\CreateNewPlayerShip;
use BinaryStudioAcademy\Game\Actions\UserChooseAction;
use BinaryStudioAcademy\Game\Contracts\Io\Reader;
use BinaryStudioAcademy\Game\Contracts\Io\Writer;
use BinaryStudioAcademy\Game\Contracts\Helpers\Random;

class Game
{
    private $random;
    public const ROOT_PATH = __DIR__;

    public function __construct(Random $random)
    {
        $this->random = $random;
    }

    public function start(Reader $reader, Writer $writer)
    {
        $playerShip = CreateNewPlayerShip::newShip();

        $writer->writeln('New game start');

        $action = new UserChooseAction($playerShip, $this->random);

        while (True){

            $writer->writeln('Enter command:');
            $input = trim($reader->read());
            if ($input === 'exit') {
                break;
            }

            $action->prepareUserInput($input);
            $action->makeAction();
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $playerShip = CreateNewPlayerShip::newShip();

        $action = new UserChooseAction($playerShip, $this->random);

        $input = trim($reader->read());
        $action->prepareUserInput($input);
        $action->makeAction();
    }
}

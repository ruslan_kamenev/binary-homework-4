<?php


namespace BinaryStudioAcademy\Game\Helpers;


class PlayerMaxStats
{
    public const STRENGTH = 10;
    public const ARMOR = 10;
    public const LUCK = 10;
    public const HEALTH = 100;
    public const CARGO = [];
}

<?php


namespace BinaryStudioAcademy\Game\Helpers;


class PlayerShipStats
{
    public const STRENGTH = 5;
    public const ARMOR = 5;
    public const LUCK = 5;
    public const HEALTH = 100;
    public const CARGO = [];
}

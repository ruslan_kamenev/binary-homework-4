<?php


namespace BinaryStudioAcademy\Game\Helpers;


use BinaryStudioAcademy\Game\Game;

class AlienShipTypeChose
{
    static function shipTypeChose(string $galaxy)
    {
        return self::findShipTypeInGalaxy($galaxy);
    }

    static function findShipTypeInGalaxy($galaxy): string
    {
        $galaxyList = include Game::ROOT_PATH.'/Data/GalaxyShipList.php';
        return $galaxyList[$galaxy];
    }
}

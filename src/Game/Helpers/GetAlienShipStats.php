<?php


namespace BinaryStudioAcademy\Game\Helpers;


use BinaryStudioAcademy\Game\Game;

class GetAlienShipStats
{
    static function getShipStats(string $shipType)
    {
        $shipList = include Game::ROOT_PATH . '/Data/AlienShipStats.php';
        return $shipList[$shipType];
    }
}

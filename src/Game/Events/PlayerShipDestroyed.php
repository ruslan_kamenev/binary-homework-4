<?php


namespace BinaryStudioAcademy\Game\Events;


use BinaryStudioAcademy\Game\Io\CliWriter;

class PlayerShipDestroyed
{
    static function shipDestroyed(): void
    {
        $write = new CliWriter();
        $write->writeln("Your spaceship got significant damages and eventually got exploded.");
        $write->writeln("You have to start from Home Galaxy.");
    }
}

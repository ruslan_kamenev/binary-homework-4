<?php


namespace BinaryStudioAcademy\Game\Events;


use BinaryStudioAcademy\Game\Io\CliWriter;
use BinaryStudioAcademy\Game\Ships\AlienShip;

class AlienShipDestroyed
{
    static function shipDestroyed(AlienShip $alienShip): void
    {
        $write = new CliWriter();
        $write->writeln("{$alienShip->getName()} is totally destroyed. Hurry up! There is could be something useful to grab.");
    }
}

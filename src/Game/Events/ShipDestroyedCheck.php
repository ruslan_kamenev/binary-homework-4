<?php


namespace BinaryStudioAcademy\Game\Events;


use BinaryStudioAcademy\Game\Ships\AlienShip;
use BinaryStudioAcademy\Game\Ships\PlayerShip;
use phpDocumentor\Reflection\Types\True_;

class ShipDestroyedCheck
{
    static function finishCheck(AlienShip $alienShip)
    {
        self::playerWin($alienShip);
        self::enemyDestroyed($alienShip);
    }

    static function playerWin(AlienShip $alienShip): void
    {
        if ($alienShip->getHealth() <= 0 and $alienShip->getName() === 'Executor') {
            PlayerWin::win();
            exit();
        }
    }

    static function playerShipHealthCheck(PlayerShip $playerShip): bool
    {
        if ($playerShip->getHealth() <= 0) {
            PlayerShipDestroyed::shipDestroyed();
            return True;
        } else {
            return False;
        }
    }

    static function enemyDestroyed(AlienShip $alienShip)
    {
        if ($alienShip->getHealth() <= 0  and $alienShip->getStatus() === 'alive') {
            $alienShip->changeStatus();
            AlienShipDestroyed::shipDestroyed($alienShip);
        }
    }
}

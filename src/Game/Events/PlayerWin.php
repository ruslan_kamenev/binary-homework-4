<?php


namespace BinaryStudioAcademy\Game\Events;


use BinaryStudioAcademy\Game\Io\CliWriter;

class PlayerWin
{
    static function win()
    {
        $writer = new CliWriter();
        $writer->writeln('🎉🎉🎉 Congratulations 🎉🎉🎉');
        $writer->writeln('🎉🎉🎉 You are winner! 🎉🎉🎉');
    }

}
